import string
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

messages = pd.read_csv('SMSSpamCollection', sep='\t', names=['label', 'message'])


def remove_punctuations(text):
    el_without_punc = [t for t in text if t not in string.punctuation]
    text_without_punc = ''.join(el_without_punc)
    list_of_words = text_without_punc.lower().split()
    return list_of_words


x_train, x_test, y_train, y_test = train_test_split(messages['message'], messages['label'], test_size=.3,
                                                    random_state=42)

pipeline = Pipeline([
    ('bag_of_words', CountVectorizer(analyzer=remove_punctuations, stop_words='english')),
    ('tfidf_transform', TfidfTransformer()),
    ('classifier', MultinomialNB())
])

pipeline.fit(x_train, y_train)

prediction = pipeline.predict(x_test)

print(classification_report(y_test, prediction))
